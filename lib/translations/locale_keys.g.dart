// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const error = 'error';
  static const login = 'login';
  static const login_text = 'login_text';
  static const validate_error_title_email = 'validate_error_title_email';
  static const validate_error_title_password = 'validate_error_title_password';
  static const validate_error_title_name = 'validate_error_title_name';
  static const validate_error_title_phone = 'validate_error_title_phone';
  static const name = 'name';
  static const phone = 'phone';
  static const email = 'email';
  static const password = 'password';
  static const to_register_screen_text = 'to_register_screen_text';
  static const to_login_screen_text = 'to_login_screen_text';
  static const button_to_login_screen = 'button_to_login_screen';
  static const button_to_register_screen = 'button_to_register_screen';
  static const register = 'register';
  static const register_text = 'register_text';
  static const home = 'home';
  static const categories = 'categories';
  static const new_product = 'new_product';
  static const favorite = 'favorite';
  static const cart = 'cart';
  static const account = 'account';
  static const your_cart = 'your_cart';
  static const settings = 'settings';
  static const about = 'about';
  static const validate_error_title = 'validate_error_title';
  static const exit = 'exit';
  static const save = 'save';
  static const discount = 'discount';
  static const profile = 'profile';
  static const input_name = 'input_name';
  static const input_email = 'input_email';
  static const input_phone = 'input_phone';
  static const empty_favorites = 'empty_favorites';
  static const empty_carts = 'empty_carts';
  static const pay = 'pay';

}
