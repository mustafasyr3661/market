// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ar = {
  "error": "مشكلة",
  "login": "سجل الدخول",
  "login_text": "سجل الان من اجل عروض مغرية",
  "validate_error_title_email": "يجب عليك ادخال بريدك الالكتروني",
  "validate_error_title_password": "يجب عليك ادخال كلمة السر ",
  "validate_error_title_name": "يجب عليك ادخال اسمك",
  "validate_error_title_phone": "يجب عليك ادخال رقم الهاتف",
  "name": "اسمك",
  "phone": "الهاتف",
  "email": "بريدك الالكتروني",
  "password": "كلمة السر",
  "to_register_screen_text": "في حال عدم امتلاك حساب",
  "to_login_screen_text": "في حال كنت تملك حساب",
  "button_to_login_screen": "سجل دخولك الان",
  "button_to_register_screen": "اشترك الان",
  "register": "انشاء حساب",
  "register_text": "اشترك الان من اجل عروض مغرية",
  "home": "الرئسية",
  "categories": "تصنيفات",
  "new_product": "منتجات جديدة",
  "favorite": "المفضلة",
  "cart": "السلة",
  "account": "الحساب",
  "your_cart": "سلتك",
  "settings": "الاعدادت",
  "about": "من نحن",
  "validate_error_title": "يجب عليك ادخال",
  "exit": "الغاء",
  "save": "حفظ",
  "discount": "تخفيض",
  "profile": "الملف الشخصي",
  "input_name": "ادخل اسمك",
  "input_email": "ادخل بريدك الالكتروني",
  "input_phone": "ادخل رقم الهاتف",
  "empty_favorites": "ليس لديك اي منتجات مفضلة",
  "empty_carts": "سلتك فارغة",
  "pay": "اشتري"
};
static const Map<String,dynamic> en = {
  "error": "Error",
  "login": "LOGIN",
  "login_text": "Login now to browse our hot offers",
  "validate_error_title_email": "you have to enter your email",
  "validate_error_title_password": "you have to enter your password",
  "validate_error_title_name": "you have to enter your name",
  "validate_error_title_phone": "you have to enter your phone",
  "phone": "Phone",
  "name": "Your name",
  "email": "Email",
  "password": "password",
  "to_register_screen_text": "If you don't have an account yet",
  "to_login_screen_text": "If you have an account",
  "button_to_register_screen": "Create one now",
  "button_to_login_screen": "Login now",
  "register": "Register",
  "register_text": "Register now to browse our hot offers",
  "home": "Home",
  "categories": "Categories",
  "new_product": "New Products",
  "favorite": "Favorites",
  "cart": "Carts",
  "account": "Account",
  "your_cart": "Your cart",
  "settings": "Settings",
  "about": "About",
  "validate_error_title": "you have to enter your",
  "exit": "CANCEL",
  "save": "SAVE",
  "discount": "DISCOUNT",
  "profile": "Profile",
  "input_name": "Enter your name",
  "input_email": "Enter your email",
  "input_phone": "Enter your phone",
  "empty_favorites": "You don't have any favorite item",
  "empty_carts": "You don't have any cart item",
  "pay": "Pay"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ar": ar, "en": en};
}
