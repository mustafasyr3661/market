import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/conestans/consets/colors.dart';

ThemeData darkTheme = ThemeData(
  //primarySwatch: MyColor.myPrimaryColorSwatch   ,
  scaffoldBackgroundColor: Color(0xff1F2326),
  iconTheme: IconThemeData(
    color: Color(0xffF0F1F2),
  ),
  appBarTheme: const AppBarTheme(
    titleSpacing: 20.0,
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarColor: Color(0xff141719),
      statusBarIconBrightness: Brightness.light,
    ),
    backgroundColor: Color(0xff141719),
    elevation: 0.0,
    titleTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 20.0,
      fontWeight: FontWeight.bold,
    ),
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: Colors.black87,
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    type: BottomNavigationBarType.fixed,
    selectedItemColor: Colors.blue,
    unselectedItemColor: Color(0xffF0F1F2),
    elevation: 20.0,
    backgroundColor: Color(0xff141719),
  ),
  textTheme: const TextTheme(
    bodyText1: TextStyle(),
  ),
  primaryColor: Colors.teal,
  navigationBarTheme: NavigationBarThemeData(
    backgroundColor: Color(0xff141719),
    labelTextStyle: MaterialStateProperty.all(
      TextStyle(color: Color(0xffF0F1F2),fontSize:10 ),
    ),
    iconTheme: MaterialStateProperty.all(
      IconThemeData(color: Color(0xffF0F1F2)),
    ),
  ),
);
ThemeData lightTheme = ThemeData(
    fontFamily: "janna",
    colorScheme:
        ColorScheme.fromSwatch().copyWith(primary: MyColor.myPrimaryColor),
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: const AppBarTheme(
      titleSpacing: 20.0,
      systemOverlayStyle: SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.blue, // Navigation bar
        statusBarColor: MyColor.myPrimaryColor,
      ),
      backgroundColor: Colors.white,
      elevation: 0.0,
      titleTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
      ),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
    ),
    iconTheme: const IconThemeData(color: Colors.white),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: MyColor.myPrimaryColor,
    ),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: MyColor.myPrimaryColor,
        unselectedItemColor: Colors.purple,
        elevation: 20.0,
        backgroundColor: Colors.white),
    );
