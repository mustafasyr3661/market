import 'package:flutter/material.dart';
import 'package:untitled/conestans/consets/string.dart';

buildTextForm({
  @required context,
  @required String? hintText,
  @required IconData? prefixIcon,
  @required TextEditingController? controller,
  String? Function(String? val)? onSaved,
  bool isShow = false,
  String? Function(String? val)? validate,
  void Function()? onSuffixIconPressed,
  IconData? suffixIcon,
  TextInputType? textInputType,
}) {
  return TextFormField(
    controller: controller,
    validator: validate,
    obscureText: isShow,
    keyboardType: textInputType,

    style: TextStyle(color: isDark ? Colors.white : Colors.black87),
    decoration: InputDecoration(
      hintText: hintText,
      hintStyle:
          Theme.of(context).textTheme.bodyText1!.copyWith(color: isDark ? Color(0xff66757F) : Colors.grey),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: isDark ? Color(0xffF0F1F2) : Colors.black87, width: 1),
          borderRadius: BorderRadius.circular(20)),
      prefixIcon: Icon(prefixIcon,color: isDark ? Color(0xff66757F) : Colors.grey,),
      suffixIcon: IconButton(
        onPressed: onSuffixIconPressed,
        icon: Icon(suffixIcon,color: isDark ? Color(0xff66757F) : Colors.grey,),
      ),
      border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1),
          borderRadius: BorderRadius.circular(20)),
    ),
  );
}
Widget buildButton(
    {@required String? text,
      @required Color? buttonColor,
      Color sideColor = Colors.black,
      Color textColor = Colors.white,
      Color shadowColor = Colors.black,
      @required var onPressed}) {
  return SizedBox(
    height: 50,
    width: double.infinity,
    child: ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text!,
        style: TextStyle(fontSize: 20, color: textColor),
      ),
      style: ElevatedButton.styleFrom(
          shadowColor: shadowColor,
          primary: buttonColor,
          side: BorderSide(color: sideColor, width: 2),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
    ),
  );
}
