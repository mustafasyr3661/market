import 'package:untitled/data/cache_helper.dart';

const String baseUrl = "https://student.valuxapps.com/api/";
// ignore: constant_identifier_names
const String LOGIN = "login";
// ignore: constant_identifier_names
const String HOME = "home";

// ignore: constant_identifier_names
const String CATEGORIES = "categories";

// ignore: constant_identifier_names
const String FAVORITES = "favorites";

// ignore: constant_identifier_names
const String PROFILE = "profile";

// ignore: constant_identifier_names
const String REGISTER = "register";

// ignore: constant_identifier_names
const String UPDATE = "update-profile";
// ignore: constant_identifier_names
const String CARTS = "carts";

const String enBoardingSharedPref = "enBoardingShPf";

const String loginSharedPref = "token";

const String darkMode="darkMode";

const String langPref="langPref";

String token = "";

late bool isDark;

String lang = isEn ? "en" : "ar";

late bool isEn;