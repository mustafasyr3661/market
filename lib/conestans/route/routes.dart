import 'package:flutter/cupertino.dart';
import 'package:untitled/data/models/home_model.dart';
import 'package:untitled/presentation/screens/login_screen.dart';
import 'package:untitled/presentation/screens/profile_screen.dart';
import 'package:untitled/presentation/screens/register_screen.dart';
import 'package:untitled/presentation/screens/shop_layout_screen.dart';
import 'package:untitled/presentation/screens/shop_screenes/search_screen.dart';
import 'package:untitled/presentation/screens/shop_screenes/sittings_screen.dart';

const loginScreen="./";
const registerScreen="./registerScreen";
const searchScreen="./searchScreen";
const profileScreen="./profileScreen";
const shopLayout="./shopLayoutScreen";
const productsDetails="productsDetails";
const sittingScreen="./sittingScreen";
ProductsModel ?productsModel;
 Map<String, Widget Function(BuildContext)>routes={
  loginScreen:(context) => const LoginScreen(),
  registerScreen:(context)=>const RegisterScreen(),
  searchScreen:(context)=>const SearchScreen(),
  profileScreen:(context)=> ProfileScreen(),
  shopLayout:(context)=>const ShopLayoutScreen(),
  sittingScreen:(context)=>const SittingsScreen(),
};

