import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/login_cubit/shop_login_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/components.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/conestans/route/routes.dart';
import 'package:untitled/data/cache_helper.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  bool isPasswordNotShow = true;
  BuildContext? cubitContext;
  IconData suffixIconState = Icons.remove_red_eye;
  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ShopLoginCubit(),
      child: BlocConsumer<ShopLoginCubit, ShopLoginState>(
        builder: (context, state) {
          cubitContext = context;
          return Scaffold(
            appBar: AppBar(),
            body: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          LocaleKeys.login.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .headline4!
                              .copyWith(
                                color: isDark ? Colors.white : Colors.black87,
                              ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          LocaleKeys.login_text.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                color: isDark ? Color(0xff66757F) : Colors.grey,
                              ),
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        buildTextForm(
                          context: context,
                          textInputType: TextInputType.emailAddress,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.validate_error_title_email.tr();
                            }
                            return null;
                          },
                          controller: emailController,
                          hintText: LocaleKeys.email.tr(),
                          isShow: false,
                          onSaved: (value) {},
                          prefixIcon: Icons.email_outlined,
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        buildTextForm(
                          context: context,
                          textInputType: TextInputType.visiblePassword,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.validate_error_title_password
                                  .tr();
                            }
                            return null;
                          },
                          controller: passwordController,
                          hintText: LocaleKeys.password.tr(),
                          isShow: isPasswordNotShow,
                          onSuffixIconPressed: () {
                            setState(() {
                              isPasswordNotShow = !isPasswordNotShow;
                            });
                          },
                          prefixIcon: Icons.lock_outlined,
                          suffixIcon: suffixIconState = isPasswordNotShow
                              ? Icons.remove_red_eye
                              : Icons.remove_red_eye_outlined,
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        buildLoadingCondition(
                            state is! ShopLoginLoadingState, context),
                        const SizedBox(
                          height: 40,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              LocaleKeys.to_register_screen_text.tr(),
                              style: TextStyle(
                                color: isDark ? Colors.white : Colors.black87,
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                //context.setLocale(Locale("en"));
                                Navigator.pushReplacementNamed(
                                    context, registerScreen);
                              },
                              child: Text(
                                  LocaleKeys.button_to_register_screen.tr()),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
        listener: (context, state) {
          buildLoginDataView(state);
        },
      ),
    );
  }

  Widget buildLoadingCondition(bool condition, cont) {
    if (condition) {
      return buildButton(
        buttonColor: isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
        onPressed: () {
          // context.setLocale(const Locale("ar"));
          print(condition);
          if (formKey.currentState!.validate()) {
            ShopLoginCubit.get(cont).userLogin(
              email: emailController.text,
              password: passwordController.text,
            );
          }
        },
        text: LocaleKeys.login.tr(),
        textColor: Colors.white,
        sideColor: isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
      );
    } else {
      return const Center(child: CircularProgressIndicator());
    }
  }

  void buildLoginDataView(ShopLoginState state) {
    if (state is ShopLoginSuccessState) {
      if (state.shopLoginModel.status) {
        print(state.shopLoginModel.message);
        CacheHelper.putString(
                loginSharedPref, state.shopLoginModel.data!.token!)
            .then((value) {
          token = state.shopLoginModel.data!.token!;
          print(token);
          Navigator.pushReplacementNamed(context, shopLayout);
        });
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.TOPSLIDE,
          title: LocaleKeys.error.tr(),
          desc: state.shopLoginModel.message,
        ).show();
        print(";;" + state.shopLoginModel.message);
      }
    }
  }
}
