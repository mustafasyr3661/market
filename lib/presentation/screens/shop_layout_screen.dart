import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/conestans/route/routes.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class ShopLayoutScreen extends StatefulWidget {
  const ShopLayoutScreen({Key? key}) : super(key: key);

  @override
  State<ShopLayoutScreen> createState() => _ShopLayoutScreenState();
}

class _ShopLayoutScreenState extends State<ShopLayoutScreen> {
  @override
  Widget build(BuildContext context) {
    List<String> appBarTitle = [
      LocaleKeys.home.tr(),
      LocaleKeys.categories.tr(),
      LocaleKeys.favorite.tr(),
      LocaleKeys.cart.tr()
    ];
    return BlocConsumer<ShopCubit, ShopState>(
        builder: (context, state) {
          return Scaffold(
            drawer: Drawer(
              backgroundColor: isDark ? Color(0xff1F2326) : Colors.white,
              child: Column(
                children: [
                  buildUserInfoDrawerHeader(context),
                  buildListTileItems(),
                ],
              ),
            ),
            appBar: AppBar(
              title: Text(appBarTitle[ShopCubit.get(context).currentIndex]),
              actions: [
                IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () {
                    Navigator.pushNamed(context, searchScreen);
                  },
                ),
              ],
            ),
            body: ShopCubit.get(context)
                .bottomScreens[ShopCubit.get(context).currentIndex],
            bottomNavigationBar: NavigationBar(
              destinations: [
                buildBottomNavItem(
                  icon: Icons.home_outlined,
                  selectedIcon: Icons.home,
                  label: LocaleKeys.home.tr(),
                ),
                buildBottomNavItem(
                  icon: Icons.apps_outlined,
                  selectedIcon: Icons.apps,
                  label: LocaleKeys.categories.tr(),
                ),
                buildBottomNavItem(
                  icon: Icons.favorite_border_outlined,
                  selectedIcon: Icons.favorite,
                  label: LocaleKeys.favorite.tr(),
                ),
                buildBottomNavItem(
                  icon: Icons.shopping_cart_outlined,
                  selectedIcon: Icons.shopping_cart,
                  label: LocaleKeys.cart.tr(),
                ),
              ],
              selectedIndex: ShopCubit.get(context).currentIndex,
              onDestinationSelected: (index) {
                ShopCubit.get(context).changeBottomNave(index);
              },
            ),
          );
        },
        listener: (context, state) {});
  }

  NavigationDestination buildBottomNavItem(
      {@required IconData? icon,
      @required String? label,
      @required IconData? selectedIcon}) {
    return NavigationDestination(
      icon: Icon(icon),
      label: label!,
      selectedIcon: Icon(selectedIcon),
    );
  }

  buildUserInfoDrawerHeader(context) {
    return UserAccountsDrawerHeader(
      decoration: BoxDecoration(
          color: isDark ? Color(0xff141719) : MyColor.myPrimaryColor),
      accountEmail: Text(
        ShopCubit.get(context).profileModel == null
            ? ""
            : ShopCubit.get(context).profileModel!.userInfo!.email!,
        style: TextStyle(fontSize: 15, color: Colors.grey[400]),
      ),
      accountName: Text(
        ShopCubit.get(context).profileModel == null
            ? ""
            : ShopCubit.get(context)
                .profileModel!
                .userInfo!
                .name!
                .toUpperCase(),
        style: TextStyle(fontSize: 20),
      ),
      currentAccountPicture: CircleAvatar(
          backgroundImage: NetworkImage(
              ShopCubit.get(context).profileModel == null
                  ? ""
                  : ShopCubit.get(context).profileModel!.userInfo!.image!)),
      otherAccountsPictures: [
        IconButton(
          onPressed: () {
            ShopCubit.get(context).changeThemeMode();
          },
          icon: Icon(Icons.wb_sunny_outlined),
        )
      ],
    );
  }

  buildListTileItems() {
    return Column(
      children: [
        ListTile(
          leading: Icon(Icons.person_outlined, color: getColor()),
          title: getListTileTitle(LocaleKeys.account.tr()),
          onTap: () {
            Navigator.pushNamed(context, profileScreen);
          },
        ),
        ListTile(
          leading: Icon(Icons.shopping_cart_outlined, color: getColor()),
          title: getListTileTitle(LocaleKeys.your_cart.tr()),
          onTap: () {},
        ),
        ListTile(
          leading: Icon(Icons.settings_outlined, color: getColor()),
          title: getListTileTitle(LocaleKeys.settings.tr()),
          onTap: () {
            Navigator.pushNamed(context, sittingScreen);
          },
        ),
        ListTile(
          leading: Icon(Icons.wallet_travel, color: getColor()),
          title: getListTileTitle(LocaleKeys.about.tr()),
          onTap: () {},
        ),
        ListTile(
          leading: Icon(
            Icons.person_outlined,
            color: getColor(),
          ),
          title: getListTileTitle("Profile"),
          onTap: () {},
        ),
      ],
    );
  }

  Widget getListTileTitle(String title) {
    return Text(
      title,
      style: TextStyle(color: isDark ? Colors.white : Colors.black87),
    );
  }

  Color getColor() {
    return isDark ? Colors.grey : Colors.black87;
  }
}
