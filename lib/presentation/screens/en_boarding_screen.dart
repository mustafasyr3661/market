import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/conestans/route/routes.dart';
import 'package:untitled/data/cache_helper.dart';

class EnBoardingModel {
  String image;
  String title;
  String body;

  EnBoardingModel({
    required this.image,
    required this.title,
    required this.body,
  });
}

class EnBoardingScreen extends StatefulWidget {
  @override
  State<EnBoardingScreen> createState() => _EnBoardingScreenState();
}

class _EnBoardingScreenState extends State<EnBoardingScreen> {
  var boardController = PageController();
  bool isEnBoardingEnding = false;
  List<EnBoardingModel> boarding = [
    EnBoardingModel(
        image: "lib/assets/images/undraw_empty_cart_co35.png",
        title: "scren1",
        body: "scren1"),
    EnBoardingModel(
        image: "lib/assets/images/undraw_shopping_app_flsj.png",
        title: "scren2",
        body: "scren2"),
    EnBoardingModel(
        image: "lib/assets/images/undraw_Successful_purchase_re_mpig.png",
        title: "scren3",
        body: "scren3"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          buildTextButton("SKIP"),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Expanded(
              child: buildPageView(),
            ),
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [
                buildIndicator(),
                const Spacer(),
                FloatingActionButton(
                  onPressed: (){
                    if (isEnBoardingEnding) {
                      CacheHelper.putBool(
                          key: enBoardingSharedPref, value: true)
                          .then((value) {
                        Navigator.pushReplacementNamed(context, loginScreen);
                      });
                    } else {
                      boardController.nextPage(
                          duration: const Duration(milliseconds: 750),
                          curve: Curves.easeInCirc);
                    }
                  },
                  child: const Icon(Icons.arrow_forward_ios),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildEnBoardingItem(EnBoardingModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Image(
            image: AssetImage(model.image),
          ),
        ),
        Text(
          model.title,
          style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          model.body,
          style: const TextStyle(
            fontSize: 18,
          ),
        ),
      ],
    );
  }

  Widget buildIndicator() {
    return SmoothPageIndicator(
      controller: boardController,
      count: boarding.length,
      effect: const ExpandingDotsEffect(
        dotColor: Colors.black,
        activeDotColor: MyColor.myPrimaryColor,
        dotHeight: 10,
        expansionFactor: 4,
        dotWidth: 10,
        spacing: 5.0,
      ),
    );
  }

  Widget buildPageView() {
    return PageView.builder(
      controller: boardController,
      onPageChanged: (index) {
        if (index == boarding.length - 1) {
          setState(() {
            isEnBoardingEnding = true;
          });
        } else {
          setState(() {
            isEnBoardingEnding = false;
          });
        }
      },
      physics: const BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        return buildEnBoardingItem(boarding[index]);
      },
      itemCount: boarding.length,
    );
  }

  Widget buildTextButton(String nameOfButton) {
    return TextButton(
      onPressed: () {
        CacheHelper.putBool(key: enBoardingSharedPref, value: true)
            .then((value) {
          Navigator.pushReplacementNamed(context, loginScreen);
        });
      },
      child:  Text(nameOfButton),
    );
  }

}
