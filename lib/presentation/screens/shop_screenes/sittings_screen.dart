import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/string.dart';

class SittingsScreen extends StatefulWidget {
  const SittingsScreen({Key? key}) : super(key: key);

  @override
  State<SittingsScreen> createState() => _SittingsScreenState();
}

class _SittingsScreenState extends State<SittingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: BlocBuilder<ShopCubit, ShopState>(
          builder: (context, state) {
            return IconButton(
              onPressed: () {
                ShopCubit.get(context).changeLang();
                if (isEn) {
                  context.setLocale(Locale("en"));
                } else {
                  context.setLocale(Locale("ar"));
                }
              },
              icon: Icon(Icons.language,color: isDark ?Colors.white:Colors.black87,),
            );
          },
        ),
      ),
    );
  }
}
