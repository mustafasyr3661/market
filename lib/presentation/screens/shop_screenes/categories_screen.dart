import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/models/categories_model.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopState>(
        builder: (context, state) {
          return buildCondition(context);
        },
        listener: (context, stat) {});
  }

  Widget buildCondition(context) {
    if (ShopCubit.get(context).categoriesModel != null) {
      return buildListCategoriesView(ShopCubit.get(context).categoriesModel);
    } else {
      return const Center(child: CircularProgressIndicator());
    }
  }

  Widget buildListCategoriesView(CategoriesModel? categoriesModel) {
    return ListView.separated(
        itemBuilder: (context, index) =>
            buildAListItem(categoriesModel!.data!.data![index]),
        separatorBuilder: (context, index) => buildListSeparator(),
        itemCount: categoriesModel!.data!.data!.length);
  }

  buildListSeparator() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        height: 0.5,
        width: double.infinity,
        color: isDark ? Colors.white : MyColor.myPrimaryColor,
      ),
    );
  }

  buildAListItem(DataModel dataModel) {
    return Row(
      children: [
        FadeInImage.assetNetwork(
          placeholder: "lib/assets/images/loading.gif",
          image: dataModel.image!,
          width: 100,
          height: 100,
        ),
        SizedBox(
          width: 10,
        ),
        Text(dataModel.name!,style: TextStyle(color: isDark ? Colors.white : Colors.black87,),),
        const Spacer(),
        IconButton(
            onPressed: () {},
            icon:  Icon(
              Icons.arrow_forward_ios,
              color: isDark ? Colors.white : Colors.black87,
            )),
      ],
    );
  }
}
