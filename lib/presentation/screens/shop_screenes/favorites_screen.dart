import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/models/favorite_model.dart';
import 'package:untitled/presentation/screens/shop_screenes/products_details_screen.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class FavoritesScreen extends StatelessWidget {
  const FavoritesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopState>(
      builder: (context, state) {
        if (ShopCubit.get(context).getFavoritesModel!.data!.data!.isEmpty) {
          return buildEmptyView();
        } else {
          return buildConditionLayout(context, state);
        }
      },
      listener: (context, state) {},
    );
  }

  Widget buildFavoriteItems(FavoritesData favoritesData, context) {
    return ListTile(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductsDetailsScreen(
              name: favoritesData.product!.name!,
              id: favoritesData.product!.id!,
              description: favoritesData.product!.description!,
              image: favoritesData.product!.image!,
            ),
          ),
        );
      },
      style: ListTileStyle.list,
      leading: Hero(
        tag: favoritesData.product!.id!,
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Image(
              image: NetworkImage(favoritesData.product!.image!),
              height: 100,
              width: 100,
            ),
            if (favoritesData.product!.discount != 0)
              Container(
                color: Colors.red,
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child:  Text(
                  LocaleKeys.discount.tr(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 8,
                  ),
                ),
              ),
          ],
        ),
      ),
      title: Text(
        favoritesData.product!.name!,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: isDark ? Colors.white : Colors.black87,
        ),
      ),
      subtitle: Row(
        children: [
          Text(
            "${favoritesData.product!.price!}",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: isDark ? Colors.blue : MyColor.myPrimaryColor,
              fontSize: 14,
              height: 1.3,
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          if (favoritesData.product!.discount != 0)
            Text(
              "${favoritesData.product!.oldPrice}",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                color: Colors.grey,
                fontSize: 10,
                height: 1.3,
                decoration: TextDecoration.lineThrough,
              ),
            ),
          const Spacer(),
          IconButton(
            splashRadius: 2,
            padding: EdgeInsets.zero,
            onPressed: () {
              ShopCubit.get(context)
                  .changeFavorites(favoritesData.product!.id!);
            },
            icon: Icon(
              ShopCubit.get(context).favorites[favoritesData.product!.id]!
                  ? Icons.favorite
                  : Icons.favorite_border,
              size: 20,
              color:
                  ShopCubit.get(context).favorites[favoritesData.product!.id]!
                      ? Colors.pink
                      : Colors.black,
            ),
          )
        ],
      ),
    );
  }

  Widget buildConditionLayout(context, state) {
    if (state is! ShopLoadingGetFavoritesDataState) {
      return ListView.builder(
        itemBuilder: (context, index) {
          return buildFavoriteItems(
              ShopCubit.get(context).getFavoritesModel!.data!.data![index],
              context);
        },
        itemCount: ShopCubit.get(context).getFavoritesModel!.data!.data!.length,
      );
    } else {
      {
        return Center(child: CircularProgressIndicator());
      }
    }
  }

  Widget buildEmptyView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset("lib/assets/images/empty.png"),
        SizedBox(
          height: 10,
        ),
          Text(
          LocaleKeys.empty_favorites.tr(),
          style: const TextStyle(fontSize: 20, color: Colors.grey),
        )
      ],
    );
  }
}
