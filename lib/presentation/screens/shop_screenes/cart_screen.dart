import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/components.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/models/cart_model.dart';
import 'package:untitled/presentation/screens/shop_screenes/products_details_screen.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class CartsScreen extends StatefulWidget {
  CartsScreen({Key? key}) : super(key: key);

  @override
  State<CartsScreen> createState() => _CartsScreenState();
}

class _CartsScreenState extends State<CartsScreen> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopState>(
      builder: (context, state) {
        if (ShopCubit.get(context)
            .getAllCartModel!
            .getCartsData!
            .cartItems!
            .isEmpty) {
          return buildEmptyView();
        } else {
          return buildConditionLayout(context, state);
        }
      },
      listener: (context, state) {},
    );
  }

  Widget buildCartItems(CartItemsModel cartItemsModel, context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProductsDetailsScreen(
                        name: cartItemsModel.productCartsModel!.name!,
                        id: cartItemsModel.productCartsModel!.id!,
                        description:
                            cartItemsModel.productCartsModel!.description!,
                        image: cartItemsModel.productCartsModel!.image!,
                      ),
                    ),
                  );
                },
                style: ListTileStyle.list,
                leading: Hero(
                  tag: cartItemsModel.productCartsModel!.id!,
                  child: Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: [
                      Image(
                        image: NetworkImage(
                            cartItemsModel.productCartsModel!.image!),
                        height: 100,
                        width: 100,
                      ),
                      if (cartItemsModel.productCartsModel!.discount != 0)
                        Container(
                          color: Colors.red,
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child:  Text(
                            LocaleKeys.discount.tr(),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 8,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                title: Text(
                  cartItemsModel.productCartsModel!.name!,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: isDark ? Colors.white : Colors.black87,
                  ),
                ),
                subtitle: Row(
                  children: [
                    Text(
                      "${cartItemsModel.productCartsModel!.price!}",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: isDark ? Colors.blue : MyColor.myPrimaryColor,
                          fontSize: 14,
                          height: 1.3),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    if (cartItemsModel.productCartsModel!.discount != 0)
                      Text(
                        "${cartItemsModel.productCartsModel!.oldPrice}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 10,
                          height: 1.3,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                    const Spacer(),
                    buildCartButton(
                        context, cartItemsModel.productCartsModel!.id!),
                  ],
                ),
              ),
            ),
            buildButtonsCart(context, cartItemsModel.productCartsModel!.id!,
                cartItemsModel.productCartsModel!.price),
          ],
        ),
        Divider(
          color: isDark ? Colors.white : MyColor.myPrimaryColor,
          thickness: 0.5,
        ),
      ],
    );
  }

  Widget buildConditionLayout(context, state) {
    if (state is! ShopSuccessGetCartsDataState) {
      return Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return buildCartItems(
                    ShopCubit.get(context)
                        .getAllCartModel!
                        .getCartsData!
                        .cartItems![index],
                    context);
              },
              itemCount: ShopCubit.get(context)
                  .getAllCartModel!
                  .getCartsData!
                  .cartItems!
                  .length,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buildButton(
              text: "${LocaleKeys.pay.tr()}💰${ShopCubit.get(context).totalPrice!}",
              buttonColor: isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
              onPressed: () {},
              sideColor: isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
            ),
          )
        ],
      );
    } else {
      {
        return Center(child: CircularProgressIndicator());
      }
    }
  }

  Widget buildEmptyView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset("lib/assets/images/empty.png"),
        SizedBox(
          height: 10,
        ),
         Text(
          LocaleKeys.empty_carts.tr(),
          style: TextStyle(fontSize: 20, color: Colors.grey),
        )
      ],
    );
  }

  buildCartButton(context, int id) {
    return IconButton(
      splashRadius: 2,
      padding: EdgeInsets.zero,
      onPressed: () {
        ShopCubit.get(context).addToCarts(id);
        ShopCubit.get(context).cartId[id]=1;
      },
      icon: Icon(
        ShopCubit.get(context).inCarts[id]!
            ? Icons.shopping_cart
            : Icons.shopping_cart_outlined,
        size: 20,
        color: ShopCubit.get(context).inCarts[id]!
            ? isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor
            : Colors.black,
      ),
    );
  }

  var canAddLecture = true;

  void get1(int i) {
    if (i == 1) {
      canAddLecture = false;
    } else {
      canAddLecture = true;
    }
  }

  void getCount(context, int productId, count) {
    print(productId);
    for (int i = 0; i < ShopCubit.get(context).ids.length; i++) {
      if (productId == ShopCubit.get(context).ids[i]) {
        ShopCubit.get(context).cartId.addAll({productId: count});
        break;
      }
    }
  }

  buildButtonsCart(context, productId, price) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              int i = ShopCubit.get(context).cartId[productId] == null
                  ? 1
                  : ShopCubit.get(context).cartId[productId]!;
              i++;
              ShopCubit.get(context).getNewPrice(price);
              setState(() {
                getCount(context, productId, i);
              });
            },
            child: CircleAvatar(
              child: const Text(
                "+",
                style: TextStyle(fontSize: 25, color: Colors.white),
              ),
              backgroundColor:
                  isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
              radius: 18,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            "${ShopCubit.get(context).cartId[productId] == null ? 1 : ShopCubit.get(context).cartId[productId]!}",
            style: TextStyle(color: isDark ? Colors.white : Colors.black87),
          ),
          const SizedBox(
            height: 5,
          ),
          InkWell(
            onTap: () {
              get1(ShopCubit.get(context).cartId[productId]!);
              if (canAddLecture) {
                int i = ShopCubit.get(context).cartId[productId] == null
                    ? 1
                    : ShopCubit.get(context).cartId[productId]!;
                i--;
                ShopCubit.get(context).getNewMinusPrice(price);
                setState(() {
                  getCount(context, productId, i);
                });
              }
            },
            child: CircleAvatar(
              child: const Text(
                "-",
                style: TextStyle(fontSize: 25, color: Colors.white),
              ),
              backgroundColor:
                  isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor,
              radius: 18,
            ),
          ),
        ],
      ),
    );
  }
}
