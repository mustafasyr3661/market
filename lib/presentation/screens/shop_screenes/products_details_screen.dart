import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/data/models/home_model.dart';

class ProductsDetailsScreen extends StatelessWidget {
  final String? image;
  final String? description;
  final String? name;
  final int? id;

  ProductsDetailsScreen(
      {Key? key,
      required this.image,
      required this.description,
      required this.name,
      required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColor.myPrimaryColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_sharp,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          name!,
          style: TextStyle(color: Colors.white, fontSize: 15),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
              tag: id!,
              child: Container(
                height: 500,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(image!),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            buildProductInfo(),
          ],
        ),
      ),
    );
  }

  buildProductInfo() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Divider(
            height: 10,
            endIndent: 0,
            color: MyColor.myPrimaryColor,
            thickness: 2,
          ),
          Text(description!),
        ],
      ),
    );
  }
}
