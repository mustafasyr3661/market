import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/models/categories_model.dart';
import 'package:untitled/data/models/home_model.dart';
import 'package:untitled/presentation/screens/shop_screenes/products_details_screen.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopState>(
      builder: (context, state) {
        return buildCondition(context);
      },
      listener: (context, state) {
        showFavoriteFlutterToast(state);
        showCartFlutterToast(state);
      },
    );
  }

  Widget buildCondition(context) {
    if (ShopCubit.get(context).homeModel != null &&
        ShopCubit.get(context).categoriesModel != null) {
      return buildHomeWidget(ShopCubit.get(context).homeModel,
          ShopCubit.get(context).categoriesModel, context);
    } else {
      return const Center(child: CircularProgressIndicator());
    }
  }

  Widget buildHomeWidget(modelOfHome, categoriesModel, context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildCarouselSlider(modelOfHome),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildTextTitle(LocaleKeys.categories.tr()),
                const SizedBox(
                  height: 10,
                ),
                buildCategoriesListView(categoriesModel),
                const SizedBox(
                  height: 10,
                ),
                buildTextTitle(LocaleKeys.new_product.tr()),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          buildGridView(modelOfHome, context),
        ],
      ),
    );
  }

  buildCarouselSlider(HomeModel modelOfHome) {
    return CarouselSlider(
      options: CarouselOptions(
          height: 250,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: true,
          viewportFraction: 1.0,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(seconds: 1),
          autoPlayCurve: Curves.fastOutSlowIn,
          scrollDirection: Axis.horizontal),
      items: modelOfHome.data.banners!
          .map(
            (e) => FadeInImage.assetNetwork(
              placeholder: 'lib/assets/images/loading.gif',
              image: e.image,
              width: double.infinity,
            ),
          )
          .toList(),
    );
  }

  buildGridView(HomeModel modelOfHome, context) {
    return Container(
      color: Colors.grey[300],
      child: GridView.count(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        crossAxisCount: 2,
        mainAxisSpacing: 1.0,
        crossAxisSpacing: 1.0,
        childAspectRatio: 1 / 1.49,
        children: List.generate(
          modelOfHome.data.products!.length,
          (index) =>
              buildProductsItem(modelOfHome.data.products![index], context),
        ),
      ),
    );
  }

  buildProductsItem(ProductsModel model, context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductsDetailsScreen(
              image: model.image,
              description: model.description,
              id: model.id,
              name: model.name,
            ),
          ),
        );
      },
      child: Container(
        color: isDark ? Color(0xff292F33) : Colors.white,
        child: Column(
          children: [
            Hero(
              tag: model.id!,
              child: Stack(
                alignment: AlignmentDirectional.bottomStart,
                children: [
                  FadeInImage.assetNetwork(
                    placeholder: 'lib/assets/images/loading.gif',
                    image: model.image!,
                    width: double.infinity,
                    height: 200,
                  ),
                  if (model.discount != 0)
                    Container(
                      color: Colors.red,
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child:  Text(
                        LocaleKeys.discount.tr(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                      ),
                    ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 9),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    model.name!,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      height: 1.3,
                      color: isDark ? Colors.white : Colors.black87,
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        "${model.price!}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style:  TextStyle(
                            color: isDark ? Colors.blue : MyColor.myPrimaryColor,
                            fontSize: 14,
                            height: 1.3),
                      ),
                      const SizedBox(
                        width: 2,
                      ),
                      if (model.discount != 0)
                        Text(
                          "${model.oldPrice!}",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            color: Colors.grey,
                            fontSize: 10,
                            height: 1.3,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      const Spacer(),
                      buildFavoriteButton(context, model),
                      buildCartButton(context, model),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildCategoriesListView(CategoriesModel categoriesModel) {
    return SizedBox(
      height: 100,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        itemBuilder: (context, index) =>
            buildCategoriesListViewItem(categoriesModel.data!.data![index]),
        itemCount: categoriesModel.data!.data!.length,
        separatorBuilder: (context, index) => const SizedBox(
          width: 10,
        ),
      ),
    );
  }

  buildCategoriesListViewItem(DataModel data) {
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        FadeInImage.assetNetwork(
          placeholder: "lib/assets/images/loading.gif",
          image: data.image!,
          width: 100,
          height: 100,
          fit: BoxFit.cover,
        ),
        Container(
          width: 100,
          color: MyColor.myPrimaryColor.withOpacity(0.9),
          child: Text(
            data.name!,
            style: const TextStyle(color: Colors.white),
            textAlign: TextAlign.center,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  buildTextTitle(title) {
    return Text(
      title,
      style: TextStyle(
          color: isDark ? Colors.white : MyColor.myPrimaryColor,
          fontSize: 24,
          fontWeight: FontWeight.w100),
    );
  }

  buildFavoriteButton(context, ProductsModel model) {
    return IconButton(
      splashRadius: 2,
      padding: EdgeInsets.zero,
      onPressed: () {
        ShopCubit.get(context).changeFavorites(model.id!);
      },
      icon: Icon(
        ShopCubit.get(context).favorites[model.id]!
            ? Icons.favorite
            : Icons.favorite_border,
        size: 20,
        color: ShopCubit.get(context).favorites[model.id]!
            ? isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor
            : isDark ? Colors.white : Colors.black87,
      ),
    );
  }

  buildCartButton(context, ProductsModel model) {
    return IconButton(
      splashRadius: 2,
      padding: EdgeInsets.zero,
      onPressed: () {
        ShopCubit.get(context).addToCarts(model.id!);
      },
      icon: Icon(
        ShopCubit.get(context).inCarts[model.id]!
            ? Icons.shopping_cart
            : Icons.shopping_cart_outlined,
        size: 20,
        color: ShopCubit.get(context).inCarts[model.id]!
            ? isDark ? Color(0xffD1D6D9) : MyColor.myPrimaryColor
            : isDark ? Colors.white : Colors.black87,
      ),
    );
  }

  void showFavoriteFlutterToast(state) {
    if(state is ShopThemeModeState){

    }
    if (state is ShopSuccessFavoritesDataState) {
      if (state.favoritesModel.status!) {
        Fluttertoast.showToast(
            msg: state.favoritesModel.massage!,
            backgroundColor: MyColor.myPrimaryColor,
            textColor: Colors.white,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 5,
            toastLength: Toast.LENGTH_LONG);
      } else {
        Fluttertoast.showToast(
            msg: state.favoritesModel.massage!,
            backgroundColor: MyColor.myPrimaryColor,
            textColor: Colors.white,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 5,
            toastLength: Toast.LENGTH_LONG);
      }
    }
  }

  void showCartFlutterToast(ShopState state) {
    if (state is ShopSuccessCartsState) {
      if (state.addToCartModel.status!) {
        Fluttertoast.showToast(
            msg: state.addToCartModel.massage!,
            backgroundColor: MyColor.myPrimaryColor,
            textColor: Colors.white,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 5,
            toastLength: Toast.LENGTH_LONG);
      } else {
        Fluttertoast.showToast(
            msg: state.addToCartModel.massage!,
            backgroundColor: MyColor.myPrimaryColor,
            textColor: Colors.white,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 5,
            toastLength: Toast.LENGTH_LONG);
      }
    }
  }
}
