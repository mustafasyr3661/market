import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/consets/colors.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/conestans/route/routes.dart';
import 'package:untitled/data/cache_helper.dart';
import 'package:untitled/translations/locale_keys.g.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final scaffoldState = GlobalKey<ScaffoldState>();

  final formState = GlobalKey<FormState>();

  String name = "Mustafa";

  TextEditingController nameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopState>(
      builder: (context, state) {
        nameController.text =
            ShopCubit.get(context).profileModel!.userInfo!.name!;
        emailController.text =
            ShopCubit.get(context).profileModel!.userInfo!.email!;
        phoneController.text =
            ShopCubit.get(context).profileModel!.userInfo!.phone!;
        return buildConditionLayout(context);
      },
      listener: (context, state) {},
    );
  }

  buildAListTileView(
    context, {
    @required String? title,
    @required String? subtitle,
    @required IconData? icon,
    @required TextInputType? keyboardType,
    @required String? titleOFSheet,
    @required TextEditingController? controller,
    @required int? maxLength,
  }) {
    return ListTile(
      leading: Icon(icon!,color: isDark ? Colors.grey : Colors.black87,),
      title: Text(
        title!,
        style: const TextStyle(color: Colors.grey, fontSize: 12),
      ),
      subtitle: Text(
        subtitle!,
        style:  TextStyle(color: isDark ? Colors.white : Colors.black87, fontSize: 15),
      ),
      trailing: IconButton(
        icon:  Icon(Icons.edit,color: isDark ? Colors.white : Colors.black87,),
        onPressed: () {
          buildBottomSheetToUpdateInfo(context, titleOFSheet, title, controller,
              keyboardType, maxLength);
        },
      ),
    );
  }

  buildBottomSheetToUpdateInfo(
      context,
      title,
      saveTitle,
      TextEditingController? controller,
      TextInputType? keyboardType,
      maxLength) {
    return scaffoldState.currentState!.showBottomSheet(
        (context) => buildSheetView(
            context, title, saveTitle, controller, keyboardType, maxLength),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        constraints: const BoxConstraints(
          maxHeight: 200,
        ),
        elevation: 10);
  }

  buildSheetView(context, String title, String saveTitle, controller,
      keyboardType, int maxLength) {
    return Container(
      color: isDark ? Color(0xff1F2326) : Colors.white60,
      padding: const EdgeInsets.all(20),
      child: Form(
        key: formState,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style:
                   TextStyle(fontSize: 18, color: isDark ? Colors.white : MyColor.myPrimaryColor),
              textAlign: TextAlign.start,
            ),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "${LocaleKeys.validate_error_title.tr()}+${saveTitle.toLowerCase()}";
                }
                return null;
              },
              keyboardType: keyboardType,
              maxLength: maxLength,
              controller: controller,
              style: TextStyle(color: isDark ? Color(0xffF0F1F2) : Colors.black87),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop(context);
                    },
                    child:  Text(LocaleKeys.exit.tr())),
                TextButton(
                    onPressed: () {
                      if (formState.currentState!.validate()) {
                        ShopCubit.get(context).updateUSerInfo(
                            email: emailController.text,
                            phone: phoneController.text,
                            name: nameController.text);
                      }
                      Navigator.pop(context);
                    },
                    child:  Text(LocaleKeys.save.tr())),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildConditionLayout(context) {
    if (ShopCubit.get(context).profileModel == null) {
      return const Center(child: CircularProgressIndicator());
    } else {
      return Scaffold(
        key: scaffoldState,
        appBar: AppBar(
          actions: [
            IconButton(
                onPressed: () {
                  CacheHelper.removeData(loginSharedPref).then((value) {
                    if (value) {
                      print(token);
                      Navigator.pop(context, shopLayout);
                      Navigator.pushReplacementNamed(context, loginScreen);
                    }
                  });
                },
                icon: const Icon(
                  Icons.logout,
                  color: Colors.white,
                ))
          ],
          backgroundColor: isDark ? Color(0xff141719) : MyColor.myPrimaryColor,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_sharp),
            onPressed: () {
              Navigator.of(context).pop(context);
            },
            color: Colors.white,
          ),
          title:  Text(
            LocaleKeys.profile.tr(),
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              buildUserPic(),
              buildUserInfoList(),
            ],
          ),
        ),
      );
    }
  }

  buildUserInfoList() {
    return Column(
      children: [
        buildAListTileView(context,
            icon: Icons.person,
            title: LocaleKeys.name.tr(),
            subtitle: nameController.text,
            titleOFSheet: LocaleKeys.input_name.tr(),
            controller: nameController,
            keyboardType: TextInputType.name,
            maxLength: 10),
        buildAListTileView(
          context,
          icon: Icons.email,
          title: LocaleKeys.email,
          subtitle: emailController.text,
          titleOFSheet: LocaleKeys.input_email.tr(),
          controller: emailController,
          keyboardType: TextInputType.emailAddress,
          maxLength: 30,
        ),
        buildAListTileView(context,
            icon: Icons.phone,
            title: LocaleKeys.phone.tr(),
            subtitle: phoneController.text,
            titleOFSheet: LocaleKeys.input_phone.tr(),
            controller: phoneController,
            keyboardType: TextInputType.phone,
            maxLength: 15),
      ],
    );
  }

  buildUserPic() {
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        CircleAvatar(
          radius: 80,
          backgroundImage: NetworkImage(
              ShopCubit.get(context).profileModel!.userInfo!.image!),
        ),
        Padding(
          padding:
          const EdgeInsetsDirectional.only(end: 10, bottom: 5),
          child: CircleAvatar(
            radius: 20,
            backgroundColor: Colors.green[200],
            child: const Icon(
              Icons.camera_alt,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }
}
