import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/api/shopping_api.dart';
import 'package:untitled/data/models/login_model.dart';

part 'shop_login_state.dart';

class ShopLoginCubit extends Cubit<ShopLoginState> {
  ShopLoginCubit() : super(ShopLoginInitial());

  static ShopLoginCubit get(context) => BlocProvider.of(context);
  late ShopLoginModel shopLoginModel;

  void userLogin({required String email, required String password}) {
    print("here in cubit");
    emit(ShopLoginLoadingState());
    ShoppingApi().postData(url: LOGIN, data: {
      "email": email,
      'password': password,
    }).then((value) {
      print("here in cubit");
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      emit(ShopLoginSuccessState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopLoginErrorState(error.toString()));
    });
  }
}
