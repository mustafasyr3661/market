import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/api/shopping_api.dart';
import 'package:untitled/data/models/login_model.dart';

part 'shop_register_state.dart';

class ShopRegisterCubit extends Cubit<ShopRegisterState> {
  ShopRegisterCubit() : super(ShopRegisterInitial());

  static ShopRegisterCubit get(context) => BlocProvider.of(context);
  late ShopLoginModel shopLoginModel;

  void userLogin({
    required String email,
    required String password,
    required String name,
    required String phone,
  }) {
    print("here in cubit");
    emit(ShopRegisterLoadingState());
    ShoppingApi().postData(url: REGISTER, data: {
      "email": email,
      'password': password,
      "name": name,
      "phone": phone,
    }).then((value) {
      print("here in cubit");
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      emit(ShopRegisterSuccessState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopRegisterErrorState(error.toString()));
    });
  }
}
