part of 'shop_cubit.dart';

@immutable
abstract class ShopState {}

class ShopInitial extends ShopState {}

class ShopChangeBottomNavState extends ShopState {}

class ShopLoadingHomeDataState extends ShopState {}

class ShopSuccessHomeDataState extends ShopState {}

class ShopErrorHomeDataState extends ShopState {}

class ShopSuccessCategoriesDataState extends ShopState {}

class ShopErrorCategoriesDataState extends ShopState {}

class ShopSuccessGetFavoritesDataState extends ShopState {}

class ShopErrorGetFavoritesDataState extends ShopState {}

class ShopLoadingGetFavoritesDataState extends ShopState {}

class ShopFavoritesDataState extends ShopState {}

class ShopSuccessFavoritesDataState extends ShopState {
  final FavoritesModel favoritesModel;

  ShopSuccessFavoritesDataState(this.favoritesModel);
}

class ShopErrorFavoritesDataState extends ShopState {}

class ShopSuccessProfileInfoDataState extends ShopState {
  final ProfileModel profileModel;
  ShopSuccessProfileInfoDataState(this.profileModel);

}

class ShopErrorProfileInfoDataState extends ShopState {}

class ShopLoadingRegisterState extends ShopState {}

class ShopSuccessRegisterState extends ShopState {
  final ProfileModel profileModel;
  ShopSuccessRegisterState(this.profileModel);

}

class ShopErrorRegisterState extends ShopState {}

class ShopToShopLayoutState extends ShopState {}

class ShopToLoginScreenState extends ShopState {}

class ShopErrorCartsState extends ShopState {}

class ShopCartsState extends ShopState{}

class ShopSuccessCartsState extends ShopState {
  final AddToCartModel addToCartModel;

  ShopSuccessCartsState(this.addToCartModel);
}

class ShopLoadingGetCartDataState extends ShopState {}

class ShopSuccessGetCartsDataState extends ShopState {}

class ShopErrorGetCartsDataState extends ShopState{}

class ShopChangePriceState extends ShopState{}
class ShopChangePriceMinusState extends ShopState{}

class ShopPriceState extends ShopState{}

class ShopThemeModeState extends ShopState{}

class ShopDarkThemeState extends ShopState{}

class ShopLightThemeState extends ShopState{}

class ShopChangeLang extends ShopState{}

class ShopChangeLangToEn extends ShopState{}

class ShopChangeLangToAr extends ShopState{}






