import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/conestans/consets/string.dart';
import 'package:untitled/data/api/shopping_api.dart';
import 'package:untitled/data/cache_helper.dart';
import 'package:untitled/data/models/cart_model.dart';
import 'package:untitled/data/models/categories_model.dart';
import 'package:untitled/data/models/favorite_model.dart';
import 'package:untitled/data/models/home_model.dart';
import 'package:untitled/data/models/profile_model.dart';
import 'package:untitled/presentation/screens/shop_screenes/categories_screen.dart';
import 'package:untitled/presentation/screens/shop_screenes/favorites_screen.dart';
import 'package:untitled/presentation/screens/shop_screenes/home_screen.dart';
import 'package:untitled/presentation/screens/shop_screenes/cart_screen.dart';

part 'shop_state.dart';

class ShopCubit extends Cubit<ShopState> {
  ShopCubit() : super(ShopInitial());

  static ShopCubit get(context) => BlocProvider.of(context);
  int currentIndex = 0;
  List<Widget> bottomScreens = [
    HomeScreen(),
    const CategoriesScreen(),
    const FavoritesScreen(),
    CartsScreen()
  ];
  HomeModel? homeModel;
  CategoriesModel? categoriesModel;
  FavoritesModel? favoritesModel;
  Map<int, bool> favorites = {};
  Map<int, bool> inCarts = {};
  Map<int, int> cartId = {};
  List<int> ids = [];
  GetFavoritesModel? getFavoritesModel;
  ProfileModel? profileModel;
  AddToCartModel? addToCartModel;
  GetAllCartModel? getAllCartModel;
  int? totalPrice;

  void changeBottomNave(int index) {
    currentIndex = index;
    emit(ShopChangeBottomNavState());
  }

  void getHomeData() {
    emit(ShopLoadingHomeDataState());
    print("your token is" + CacheHelper.getString(loginSharedPref));
    ShoppingApi()
        .getData(
      url: HOME,
      token: token,
      lang: lang,
    )
        .then((value) {
      homeModel = HomeModel.fromJson(value.data);
      for (var element in homeModel!.data.products!) {
        favorites.addAll(
          {
            element.id!: element.inFavorites!,
          },
        );
      }
      for (var element in homeModel!.data.products!) {
        inCarts.addAll(
          {
            element.id!: element.inCart!,
          },
        );
      }
      for (var element in homeModel!.data.products!) {
        ids.add(element.id!);
      }
      totalPrice = getAllCartModel!.getCartsData!.total!;
      print(totalPrice);
      emit(ShopSuccessHomeDataState());
    }).catchError((error) {
      print("Error??" + error.toString());
      emit(ShopErrorHomeDataState());
    });
  }

  void getCategories() {
    ShoppingApi()
        .getData(
      url: CATEGORIES,
      token: token,
      lang: lang,
    )
        .then((value) {
      categoriesModel = CategoriesModel.fromJson(value.data);
      emit(ShopSuccessCategoriesDataState());
    }).catchError((error) {
      print("Categories error" + error.toString());
      emit(ShopErrorCategoriesDataState());
    });
  }

  void changeFavorites(int productId) {
    favorites[productId] = !favorites[productId]!;
    emit(ShopFavoritesDataState());

    ShoppingApi()
        .postData(
      url: FAVORITES,
      data: {
        "product_id": productId,
      },
      token: token,
    )
        .then((value) {
      favoritesModel = FavoritesModel.fromJson(value.data);
      print(favoritesModel!.massage);
      print(favoritesModel!.status);
      if (!favoritesModel!.status!) {
        favorites[productId] = !favorites[productId]!;
      } else {
        getFavoritesData();
      }

      emit(ShopSuccessFavoritesDataState(favoritesModel!));
    }).catchError((error) {
      print(error.toString());
      favorites[productId] = !favorites[productId]!;
      emit(ShopErrorFavoritesDataState());
    });
  }

  void getFavoritesData() {
    emit(ShopLoadingGetFavoritesDataState());
    ShoppingApi()
        .getData(
      url: FAVORITES,
      token: token,
      lang: lang,
    )
        .then((value) {
      getFavoritesModel = GetFavoritesModel.fromJson(value.data);
      emit(ShopSuccessGetFavoritesDataState());
    }).catchError((error) {
      print("favorite error" + error.toString());
      emit(ShopErrorGetFavoritesDataState());
    });
  }

  getUSerInfo() {
    ShoppingApi()
        .getData(
      url: PROFILE,
      token: token,
      lang: lang,

    )
        .then((value) {
      profileModel = ProfileModel.fromJson(value.data);
      print("Your token is" + profileModel!.userInfo!.token!);
      emit(ShopSuccessProfileInfoDataState(profileModel!));
    }).catchError((error) {
      print("profile error" + error.toString());
      emit(ShopErrorProfileInfoDataState());
    });
  }

  updateUSerInfo({
    @required String? email,
    @required String? phone,
    @required String? name,
  }) {
    ShoppingApi().putData(
      url: UPDATE,
      token: token,
      lang: lang,

      data: {'name': name, 'email': email, 'phone': phone},
    ).then((value) {
      profileModel = ProfileModel.fromJson(value.data);
      print("Your token is" + profileModel!.userInfo!.token!);
      emit(ShopSuccessProfileInfoDataState(profileModel!));
    }).catchError((error) {
      print("profile error" + error.toString());
      emit(ShopErrorProfileInfoDataState());
    });
  }

  addToCarts(int productId) {
    inCarts[productId] = !inCarts[productId]!;
    emit(ShopCartsState());

    ShoppingApi()
        .postData(
      url: CARTS,
      data: {
        "product_id": productId,
      },
      token: token,

    )
        .then((value) {
      addToCartModel = AddToCartModel.fromJson(value.data);
      print(addToCartModel!.massage);
      print(addToCartModel!.status);
      if (!addToCartModel!.status!) {
        favorites[productId] = !favorites[productId]!;
      } else {
        print(totalPrice);
        getAllCartsItems();
      }

      emit(ShopSuccessCartsState(addToCartModel!));
    }).catchError((error) {
      print(error.toString());
      inCarts[productId] = !inCarts[productId]!;
      emit(ShopErrorCartsState());
    });
  }

  getAllCartsItems() {
    emit(ShopLoadingGetCartDataState());
    ShoppingApi()
        .getData(
      url: CARTS,
      token: token,
      lang: lang,

    )
        .then((value) {
      getAllCartModel = GetAllCartModel.fromJson(value.data);
      getPrice();
      emit(ShopSuccessGetCartsDataState());
    }).catchError((error) {
      print("cart error" + error.toString());
      emit(ShopErrorGetCartsDataState());
    });
  }

  void getNewPrice(price) {
    totalPrice = (totalPrice! + price) as int?;
    emit(ShopChangePriceState());
  }
  void getNewMinusPrice(price) {
    totalPrice = (totalPrice! - price) as int?;
    emit(ShopChangePriceMinusState());
  }

  void getPrice()
  {
    totalPrice= getAllCartModel!.getCartsData!.total! as int;
    emit(ShopPriceState());
  }

  void changeThemeMode({bool? themeModeForApp})
  {
    if(themeModeForApp != null)
      {
        themeMode(themeModeForApp);
        print("your dark mode is "+ "$themeModeForApp");
        emit(ShopThemeModeState());
      }
    else
      {
        isDark=!isDark;
        themeMode(isDark);
        CacheHelper.putBool(key: darkMode, value: isDark).then((value) {
          print("your dark mode is "+ "$isDark");
          emit(ShopThemeModeState());
        });
      }
  }
  ThemeMode?themeModeForAllApp;
  void themeMode(isDark){
    if(isDark){
      themeModeForAllApp=ThemeMode.dark;
      emit(ShopDarkThemeState());
    }else{
      themeModeForAllApp=ThemeMode.light;
      emit(ShopLightThemeState());
    }
  }
  void changeLang({bool? isLangType}) {
    if(isLangType!=null)
      {
        if(isLangType) {
          lang="en";
          getHomeData();
          getCategories();
          getFavoritesData();
          getUSerInfo();
          getAllCartsItems();
          emit(ShopChangeLangToEn());
        }
        else{
          lang="ar";
          getHomeData();
          getCategories();
          getFavoritesData();
          getUSerInfo();
          getAllCartsItems();
          emit(ShopChangeLangToAr());
        }
      }
    else
      {
        isEn=!isEn;
      emit(ShopChangeLang());
      if(isEn) {
          CacheHelper.putBool(key: langPref, value: isEn);
          lang="en";
          getHomeData();
          getCategories();
          getFavoritesData();
          getUSerInfo();
          getAllCartsItems();
          emit(ShopChangeLangToEn());
        }
        else{
          CacheHelper.putBool(key: langPref, value: isEn);
          lang="ar";
          getHomeData();
          getCategories();
          getFavoritesData();
          getUSerInfo();
          getAllCartsItems();
          emit(ShopChangeLangToAr());
        }
      }

  }
}
