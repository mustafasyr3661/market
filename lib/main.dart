import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:untitled/business_logic/shop_cubit/shop_cubit.dart';
import 'package:untitled/conestans/theme/theme_mode.dart';
import 'package:untitled/data/cache_helper.dart';
import 'package:untitled/presentation/screens/en_boarding_screen.dart';
import 'package:untitled/presentation/screens/login_screen.dart';
import 'package:untitled/presentation/screens/shop_layout_screen.dart';
import 'package:untitled/translations/codegen_loader.g.dart';

import 'conestans/consets/string.dart';
import 'conestans/route/routes.dart';

void main() async {
  Widget? widget;
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await CacheHelper.init();
  isDark = CacheHelper.getBool(darkMode);
  isEn = CacheHelper.getBool(langPref);
  if (CacheHelper.getBool(enBoardingSharedPref)) {
    if (CacheHelper.getString(loginSharedPref).isNotEmpty) {
      print(token);
      token = CacheHelper.getString(loginSharedPref);
      widget = const ShopLayoutScreen();
    } else {
      widget = const LoginScreen();
    }
  } else {
    widget = EnBoardingScreen();
  }
  runApp(EasyLocalization(
    path: "lib/assets/translations",
    supportedLocales: const [
      Locale("en"),
      Locale("ar"),
    ],
    fallbackLocale: const Locale("en"),
    assetLoader: const CodegenLoader(),
    child: MyApp(
      isDarkMode: isDark,
      startWidget: widget,
      isLangEng: isEn,
    ),
  ));
}

class MyApp extends StatelessWidget {
  final Widget startWidget;
  final bool isDarkMode;
  final bool isLangEng;

  MyApp(
      {required this.startWidget,
      required this.isDarkMode,
      required this.isLangEng});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ShopCubit()
            ..getHomeData()
            ..getCategories()
            ..getFavoritesData()
            ..getUSerInfo()
            ..getAllCartsItems()
            ..changeThemeMode(themeModeForApp: isDarkMode)
            ..changeLang(isLangType: isLangEng),
        ),
      ],
      child: BlocBuilder<ShopCubit, ShopState>(
        builder: (context, state) {
          return MaterialApp(
            routes: routes,
            theme: lightTheme,
            darkTheme: darkTheme,
            themeMode: ShopCubit.get(context).themeModeForAllApp,
            debugShowCheckedModeBanner: false,
            supportedLocales: context.supportedLocales,
            localizationsDelegates: context.localizationDelegates,
            locale: context.locale,
            home: startWidget,
          );
        },
      ),
    );
  }
}
