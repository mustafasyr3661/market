import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CacheHelper {
  static SharedPreferences? sharedPreferences;

  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static Future<bool?> putBool({
    @required String? key,
    @required dynamic value,
  }) async {
    if (sharedPreferences != null) return sharedPreferences!.setBool(key!, value);
  }

  static putInteger(String key, int value) {
    if (sharedPreferences != null) sharedPreferences!.setInt(key, value);
  }

  static int getInteger(String key) {
    return sharedPreferences == null ? 0 : sharedPreferences!.getInt(key) ?? 0;
  }

  static Future<String?> putString(String key, String value) async {
    if (sharedPreferences != null) sharedPreferences!.setString(key, value);
  }

  static String getString(String key) {
    return  sharedPreferences == null
        ? 'DEFAULT_VALUE'
        : sharedPreferences!.getString(key) ?? "";
  }

  static bool getBool(String key) {
    return sharedPreferences == null
        ? false
        : sharedPreferences!.getBool(key) ?? false;
  }

  static Future<bool> removeData(String key)async {
    return await sharedPreferences!.remove(key);
  }
}
