import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:untitled/conestans/consets/string.dart';

class ShoppingApi {
  late Dio dio;

  ShoppingApi() {
    BaseOptions options = BaseOptions(
        baseUrl: baseUrl,
        receiveDataWhenStatusError: true,
        connectTimeout: 60 * 1000,
        receiveTimeout: 20 * 1000);
    dio = Dio(options);
  }

  Future<Response> postData(
      {@required String? url,
      Map<String, dynamic>? query,
      String? token,
      @required Map<String, dynamic>? data}) async {
    dio.options.headers = {
      'Content-Type': 'application/json',
      "lang": lang,
      'Authorization': token ?? '',
    };
    return dio.post(
      url!,
      data: data,
      queryParameters: query,
    );
  }

  Future<Response> getData({
    @required String? url,
    Map<String, dynamic>? query,
    String lang = "en",
    String? token,
  }) async {
    dio.options.headers = {
      'lang': lang,
      'Content-Type': 'application/json',
      'Authorization': token ?? '',
    };
    return dio.get(
      url!,
      queryParameters: query,
    );
  }

  Future<Response> putData(
      {@required String? url,
        Map<String, dynamic>? query,
        String? token,
        String lang = "en",
        @required Map<String, dynamic>? data}) async {
    dio.options.headers = {
      'Content-Type': 'application/json',
      "lang": lang,
      'Authorization': token ?? '',
    };
    return dio.put(
      url!,
      data: data,
      queryParameters: query,
    );
  }
}
