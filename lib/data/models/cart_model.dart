class AddToCartModel {
  bool? status;
  String? massage;

  AddToCartModel.fromJson(Map<String, dynamic> json) {
    status = json["status"];
    massage = json["message"];
  }
}

class GetAllCartModel {
  bool? status;
  GetCartsData? getCartsData;

  GetAllCartModel.fromJson(Map<String, dynamic> json) {
    status = json["status"];
    getCartsData =
        json['data'] != null ? GetCartsData.fromJson(json["data"]) : null;
  }
}

class GetCartsData {
  var subTotal;
  var total;
  List<CartItemsModel>? cartItems;

  GetCartsData.fromJson(Map<String, dynamic> json) {
    subTotal = json["sub_total"];
    total = json["sub_total"];
    if (json['cart_items'] != null) {
      cartItems = <CartItemsModel>[];
      json['cart_items'].forEach((v) {
        cartItems!.add(new CartItemsModel.fromJson(v));
      });
    }
  }
}

class CartItemsModel {
  int? id;
  int? quantity;
  ProductCartsModel? productCartsModel;

  CartItemsModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    quantity = json["quantity"];
    productCartsModel = ProductCartsModel.fromJson(json["product"]);
  }
}

class ProductCartsModel {



  int cunt=1;
  int? id;
  var price;
  var oldPrice;
  var discount;
  String? image;
  String? name;
  String? description;
  bool? inFavorites;
  bool? inCart;

  ProductCartsModel();

  ProductCartsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    price = json['price'];
    oldPrice = json['old_price'];
    discount = json['discount'];
    image = json['image'];
    name = json['name'];
    description = json['description'];
    inFavorites = json['in_favorites'];
    inCart = json['in_cart'];
  }
}
