class ProfileModel{
  bool? status;
  UserInfo ? userInfo;
  ProfileModel.fromJson(Map<String,dynamic>json)
  {
    status=json["status"];
    userInfo=UserInfo.fromJson(json["data"]);
  }
}

class UserInfo{
  int ?id;
  String?name;
  String ?email;
  String ?phone;
  String?image;
  String ?token;
  UserInfo.fromJson(Map<String,dynamic>json)
  {
    id=json["id"];
    name=json["name"];
    email=json["email"];
    phone=json["phone"];
    image=json["image"];
  }
}